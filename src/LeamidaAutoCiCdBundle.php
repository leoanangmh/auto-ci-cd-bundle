<?php

namespace Leamida\AutoCiCdBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class LeamidaAutoCiCdBundle extends AbstractBundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
    public function getContainerExtension(): null|ExtensionInterface
    {
        return new DependencyInjection\AutoCiCdExtension();
    }
}
