<?php

namespace Leamida\AutoCiCdBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('auto_ci_cd');

        $treeBuilder->getRootNode()
            ->children()
            ->scalarNode('repository_url')->isRequired()->end()
            ->scalarNode('branch')->defaultValue('main')->end()
            ->end();

        return $treeBuilder;
    }
}
