<?php

namespace Leamida\AutoCiCdBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpFoundation\Response;

#[AsController]
class WebhookController extends AbstractController
{
    private ParameterBagInterface $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    #[Route('/gitlab-webhook', name: 'leamida_auto_ci_cd_index', methods: ['GET'])]
    public function handleWebhook(Request $request): JsonResponse
    {
        $this->updateRepository();
        return new JsonResponse(['status' => 'ok']);
    }

    private function updateRepository()
    {
        $projectDir = dirname(dirname(dirname(dirname(dirname(__DIR__)))));
        $parts = explode('/', $projectDir);

        // Find the position of the last occurrence of any directory name
        $lastDirectory = end($parts);

        $newDir = $projectDir . '/../' . $lastDirectory . '_' . 'new';

        // Rename current project folder with version

        // Clone the repository
        // Clone the repository
        $branch = escapeshellarg($this->params->get('auto_ci_cd.branch'));
        $repositoryUrl = escapeshellarg($this->params->get('auto_ci_cd.repository_url'));
        $cloneDir = escapeshellarg($newDir);

        $cloneCommand = sprintf('git clone -b %s %s %s', $branch, $repositoryUrl, $cloneDir);
        shell_exec($cloneCommand);
        shell_exec("chmod -R 777 $newDir/var");

        rename($newDir, $projectDir . '/../' . $lastDirectory);
    }
}
